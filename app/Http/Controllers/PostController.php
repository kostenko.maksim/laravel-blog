<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index','show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search ?? '';

        if ($search) {
            $posts = Post::Join('users','author_id','=','users.id')
                //->select('posts.id as id')
                ->where('title','like','%'.$search.'%')
                ->orWhere('description','like','%'.$search.'%')
                ->orWhere('name','like','%'.$search.'%')
                ->orderBy('posts.created_at','desc')
                ->paginate(6);

        } else {
            $posts = Post::Join('users','author_id','=','users.id')
                ->select('*','posts.id as id')
                ->orderBy('posts.created_at','desc')
                ->paginate(6);
        }

        return view('posts.index', compact('posts','search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create',['post' => null]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $post = New Post();
        $post->title = $request->title;
        $post->short_title = Str::length($request->title)>30 ? Str::substr($request->title,0,30).'...' : $request->title;
        $post->description = $request->description;
        $post->author_id = \Auth::User()->id;

        if ($request->file('image')) {
            $path = Storage::putFile('public', $request->file('image'));
            $post->image = Storage::url($path);
        }
        $post->save();

        return redirect()->route('posts.index')->with('success','Пост '.$request->title.' успешно создан');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::join('users','author_id','=','users.id')
            ->select('*','posts.id as id')
            ->find($id);

        if(empty($post))
            abort('404');

        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        if (\Auth::User()->id != $post->author_id)
            return redirect()->route('posts.index')->withErrors('Вы не можете редактировать данную статью');

        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        $post = Post::find($id);

        if (\Auth::User()->id != $post->author_id)
            return redirect()->route('posts.index')->withErrors('Вы не можете редактировать данную статью');

        $post->title = $request->title;
        $post->short_title = Str::length($request->title)>30 ? Str::substr($request->title,0,30).'...' : $request->title;
        $post->description = $request->description;

        if ($request->file('image')) {
            $path = Storage::putFile('public', $request->file('image'));
            $post->image = Storage::url($path);
        }
        $post->update();

        return redirect()->route('posts.show',['post'=> $id])->with('success','Пост '.$request->title.' успешно изменен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        if (\Auth::User()->id != $post->author_id)
            return redirect()->route('posts.index')->withErrors('Вы не можете редактировать данную статью');

        $post->delete();
        return redirect()->route('posts.index')->with('success','Пост успешно удален');
    }
}
