<?php

use Illuminate\Support\Facades\Route,
    App\Http\Controllers\PostController,
    App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'index']);
Route::resource('posts', PostController::class);
Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');


/*Route::get('/posts/', [PostController::class, 'index'])->name('post.index');
Route::get('/posts/create', [PostController::class, 'create'])->name('post.create');
Route::get('/posts/show/{id}', [PostController::class, 'show'])->name('post.show');
Route::get('/posts/edit/{id}', [PostController::class, 'edit'])->name('post.edit');
Route::patch('/posts/show/{id}', [PostController::class, 'update'])->name('post.update');
Route::delete('/posts/destroy/{id}', [PostController::class, 'destroy'])->name('post.destroy');
Route::post('/posts', [PostController::class, 'store'])->name('post.store');*/


