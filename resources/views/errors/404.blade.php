@extends('layouts.layout',['title' => '404 Not Found' ])

@section('content')
    <div class="relative d-flex flex-column justify-content-center align-items-center h-100 ">
                <div class="h1 px-4 text-lg text-gray-500 border-r border-gray-400 tracking-wider">
                    404                    </div>

                <div class="ml-4 mb-4 text-lg text-gray-500 uppercase tracking-wider">
                    Not Found                     </div>
        <a href="{{ route('posts.index') }}" class="btn btn-outline-primary me-2">На главную</a>
    </div>
@endsection
