@extends('layouts.layout', ['title' => 'Главная'])

@section('content')

    @if($search)
        <div class="search-result mb-3">
            По запросу {{ $search }} найдено {{ count($posts) }} шт. статей
        </div>

    @endif

    <div class="row">
        @foreach($posts as $post)
            <div class="col-sm-4 mb-5">
                <?php // echo '<pre>',var_dump($post),'</pre>'; ?>
                <div class="card h-100">
                    <div class="card-header"><h4>{{ $post->short_title }}</h4></div>
                    <div class="card-body">
                        <div class="card-img" style="background-image: url({{ $post->image ?? asset('img/no_post.png') }})"></div>
                        <div class="card-author">Автор: {{ $post->name }}</div>
                        <a href="{{ route('posts.show',['post' => $post->id]) }}" class="btn btn-outline-primary">Посмотреть пост</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

{{--    @if(isset($posts->pageName))--}}
        {{ $posts->links('vendor.pagination.bootstrap-4') }}
{{--    @endif--}}
@endsection
