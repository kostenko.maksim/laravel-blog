<div class="mb-3">
    <label for="titlePost" class="form-label">Название</label>
    <input type="text" class="form-control" id="titlePost" name="title" value="{{ old('title') ?? $post->title ?? '' }}">
</div>
<div class="mb-3">
    <label for="descriptionPost" class="form-label">Текст статьи</label>
    <textarea type="text" rows="5" class="form-control" id="descriptionPost" name="description">{{ old('description') ?? $post->description ?? '' }}</textarea>
</div>
<div class="mb-3">
    <label for="imagePost" class="form-label">Изображение</label>
    <input type="file" class="form-control" id="imagePost" name="image">
    @if($post && $post->image)
        <img src="{{ $post->image }}" alt="Изображение">
    @endif
</div>
