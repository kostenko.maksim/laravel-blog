@extends('layouts.layout', ['title' => $post->short_title])

@section('content')

    <div class="row">
        <div class="col-sm-12 mb-5">
            <div class="card h-100 {{ App::currentLocale() }}">
                <div class="card-header"><h4>{{ $post->title }}</h4></div>
                <div class="card-body">
                    <div class="card-img card-img__show" style="background-image: url({{ $post->image ?? asset('img/no_post.png') }})"></div>
                    <div class="card-description">
                        {{ $post->description }}
                    </div>
                    <div class="card-author">Автор: {{ $post->name }}</div>
                    <div class="card-date mb-3">
                        Опубликован: <span title="{{ $post->created_at->format('d.m.Y H:i:s') }}">
                            {{ $post->created_at->diffForHumans() }}
                        </span>
                    </div>
                    <div class="card-btn d-flex">
                        <a href="{{ route('posts.index') }}" class="btn btn-outline-primary me-2">На главную</a>
                        @auth()
                            @if(Auth::user()->id == $post->author_id)
                            <a href="{{ route('posts.edit',['post'=> $post->id]) }}" class="btn btn-outline-success me-2">Редактировать</a>
                            <form action="{{ route('posts.destroy', ['post'=> $post->id]) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-outline-danger">Удалить</button>
                            </form>
                            @endif
                        @endauth
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
