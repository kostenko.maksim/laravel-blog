@extends('layouts.layout' , ['title' => 'Создание статьи'])

@section('content')
    <form action="{{ route('posts.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <h3>Создать статью</h3>
        @include('posts.parts.form')
        <button type="submit" class="btn btn-primary">Создать</button>
    </form>
@endsection
