@extends('layouts.layout', ['title' => $post->short_title])

@section('content')
    <form action="{{ route('posts.update', ['post' => $post->id]) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <h3>Редактирование статьи</h3>
        @include('posts.parts.form')
        <button type="submit" class="btn btn-success">Сохранить</button>
        <a href="{{ route('posts.show', ['post' => $post->id]) }}" class="btn btn-primary">Назад</a>
    </form>
@endsection
