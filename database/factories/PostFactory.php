<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->realText(rand(10,40));
        $shortTitle = mb_strlen($title) > 40 ? mb_substr($title,0,30).'...' : $title;
        $created = $this->faker->dateTimeBetween('-30 days','-1 days');

        return [
            'author_id' => rand(1,4),
            'title' => $title,
            'short_title' => $shortTitle,
            'description' => $this->faker->realText(rand(150,500)),
            'created_at' => $created,
            'updated_at' => $created,
        ];
    }
}
