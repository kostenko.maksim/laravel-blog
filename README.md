<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## Сайт-блог на Laravel

1. Авторизация и регистрация 
2. На главной странице вывод списка всех статьей с пагинацией
3. Поиск по статьям
4. Создание, просмотр, редактирование, удаление статьи
5. Редактирование и удаление только если пользователь автор статьи

### Установка composer
```
php composer-setup.php --install-dir=bin --filename=composer
mv composer.phar /usr/bin/composer
```

### Установка Laravel
```
composer create-project laravel/laravel laravel
```

### Install Bootstrap in Laravel
```
composer require laravel/ui
php artisan ui bootstrap
php artisan ui bootstrap --auth
npm install & npm run dev
```


### Установка Laravel Debugbar
```
composer require barryvdh/laravel-debugbar --dev
```

### Установка Laravel IDE Helper Generator
```
composer require barryvdh/laravel-ide-helper --dev 
```

### Создание модели Post
```
php artisan make:model Post -m
```

### Выполнение миграций с предварительным дропом всех таблиц
```
php artisan migrate:fresh
```
### Создаем фактори-генератор контента
```
php artisan make:factory PostFactory --model=Post
```

### Выполнение миграций с предварительным дропом всех таблиц и заполнением данными
```
php artisan migrate:fresh --seed
```

### Создание контроллера 
```
php artisan make:controller PostController
```

### или "ресурсного контроллера" (контроллера с готовыми методами для удобства)
```
php artisan make:controller PostController --resource
```

### Самый простой способ отредактировать шаблоны постраничной навигации – это экспортировать их в каталог resources/views/vendor с помощью команды vendor:publish:
```
php artisan vendor:publish --tag=laravel-pagination
Copied Directory [/vendor/laravel/framework/src/Illuminate/Pagination/resources/views] To [/resources/views/vendor/pagination]

```

### Проверка и очистка кеша роутинга
```
php artisan route:list
php artisan route:clear
```


### Чтобы сделать эти файлы доступными из интернета, вы должны создать символическую ссылку на storage/app/public в public/storage.
```
php artisan storage:link
```

### Создать файл кэша для более быстрой загрузки конфигурации. Это для конфигурационного кэша. Эта команда очистит конфигурационный кэш до его создания.
```
php artisan config:cache
```

### Очистить кэша
```
php artisan config:clear
```
### Создаем Request для валидации модели post
```
php artisan make:request PostRequest
```


